package common

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq" 

)

type Database struct {
	*gorm.DB
}

var DB *gorm.DB

func Init() *gorm.DB {
	address :=  "host=localhost user=postgres password=1 dbname=demo port=5432 sslmode=disable"
	db, err := gorm.Open("postgres", address)
	if err != nil {
		fmt.Println("Can not connected database due to ", err)
	}
	DB = db
	return DB
}

func GetDB() *gorm.DB {
	return DB
}
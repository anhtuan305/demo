package users

import (
	"demo/common"
	"fmt"

	"github.com/gin-gonic/gin"
)

type UserSerializer struct {
	c *gin.Context
}

func (self *ProfileSerializer) Response() ProfileResponse {
	myUserModel := self.C.MustGet("my_user_model").(UserModel)
	profile := ProfileResponse{
		ID:        self.ID,
		Username:  self.Username,
		Bio:       self.Bio,
		Image:     self.Image,
		Following: myUserModel.isFollowing(self.UserModel),
	}
	return profile
}

type ProfileSerializer struct {
	C *gin.Context
	UserModel
}

type UserResponese struct {
	Username string  `json:"username"`
	Email    string  `json:"email"`
	Bio      string  `json:"bio"`
	Image    *string `json:"image"`
	Token    string  `json:"token"`
}

func (self *UserSerializer) Response() UserResponese {
	myUserModel := self.c.MustGet("my_user_model").(UserModel)
	fmt.Print("-----", myUserModel)
	user := UserResponese{
		Username: myUserModel.Username,
		Email:    myUserModel.Email,
		Bio:      myUserModel.Bio,
		Image:    myUserModel.Image,
		Token:    common.GenToken(myUserModel.ID),
	}
	return user
}

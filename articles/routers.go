package articles

import (
	"demo/common"
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
)

func ArticleRegister(router *gin.RouterGroup) {
	router.POST("/", ArticleCreate)
}

func ArticlesAnonymousRegister(router *gin.RouterGroup) {
	router.GET("/", ArticleList)
}

func ArticleCreate(c *gin.Context) {

}

func ArticleList(c *gin.Context) {
	tag := c.Query("tag")
	author := c.Query("author")
	favorited := c.Query("favorited")
	limit := c.Query("limit")
	offset := c.Query("offset")
	articleModels, modelCount, err := FindManyArticle(tag, author, limit, offset, favorited)
	if err != nil {
		c.JSON(http.StatusNotFound, common.NewError("articles", errors.New("Invalid param")))
		return
	}
	serializer := ArticlesSerializer{c, articleModels}
	c.JSON(http.StatusOK, gin.H{"articles": serializer.Response(), "articlesCount": modelCount})
}

package main

import (
	"demo/articles"
	"demo/common"
	"demo/users"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

func Migrate(db *gorm.DB) {
	users.AutoMigrate()
	db.AutoMigrate(&articles.ArticleModel{})
	db.AutoMigrate(&articles.TagModel{})
	db.AutoMigrate(&articles.FavoriteModel{})
	db.AutoMigrate(&articles.ArticleUserModel{})
	db.AutoMigrate(&articles.CommentModel{})
}

func main() {
	db := common.Init()
	Migrate(db)
	defer db.Close()
	router := gin.Default()
	v1 := router.Group("/api")
	users.UsersRegister(v1.Group("/users"))
	v1.Use(users.AuthMiddeware(false))
	articles.ArticlesAnonymousRegister(v1.Group("/articles"))
	router.Run(":8081")
}

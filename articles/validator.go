package articles

import (
	_ "github.com/gin-gonic/gin"
)

type ArticleModelValidator struct {
	Article struct {
		Title       string   `json:"title"`
		Description string   `json:"description"`
		Body        string   `json:"body" `
		Tags        []string `json:"tagList"`
	} `json:"article"`
	articleModel ArticleModel `json:"-"`
}

func NewArticleModelValidator() ArticleModelValidator {
	return ArticleModelValidator{}
}
